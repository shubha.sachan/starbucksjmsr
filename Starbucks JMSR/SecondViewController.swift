//
//  SecondViewController.swift
//  Starbucks JMSR
//
//  Created by Shubha Sachan on 09/11/20.
//

import UIKit

class SecondViewController: UIViewController, TokenManagerDelegate {
    
    var beToken : String = ""
    
    var viewController = ViewController()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewController.tokenDelegate = self
        //viewController.loadData()
    }
    
    func passData(token: String) {
        print("hello2")
        print("pass:",token)
    }
    
    
    @IBAction func logOutPressed(_ sender: UIBarButtonItem) {
        logOutRequest()
        
    }
    
    func logOutRequest(){
        let Url = String(format: "http://localhost:8080/api/v1/signout")
            guard let serviceUrl = URL(string: Url) else { return }

            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "POST"
            request.setValue("TCP", forHTTPHeaderField: "client_id")
            request.setValue("test", forHTTPHeaderField: "client_secret")
           // request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("Bearer eadf234e-2d78-406f-9832-4d10dc713715", forHTTPHeaderField: "Authorization")
//            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters) else {
//             return
//            }
           // request.httpBody = httpBody
            //request.timeoutInterval = 20
            //print("Hello:", request)
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                
                if let response = response {
                   // print(response)
                }
                if let data = data {
                    let getData = String(data : data ,encoding: .utf8)
                   print(getData!)
                }
                
            }.resume()
        }

}
