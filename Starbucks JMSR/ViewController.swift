//
//  ViewController.swift
//  Starbucks JMSR
//
//  Created by Shubha Sachan on 19/10/20.
//

import UIKit
import AuthenticationServices

protocol TokenManagerDelegate {
    func passData(token : String)
}

class ViewController: UIViewController,ASWebAuthenticationPresentationContextProviding {
    
    var tokenDelegate : TokenManagerDelegate?
    
    var session: ASWebAuthenticationSession?
    
    var btoken = ""
    
    @IBOutlet weak var discriptionLabel: UILabel!
    
    @IBOutlet weak var loremIpsumTextLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        discriptionLabel.text = "Become a member of Starbucks Reward program and earn points"
        
        loremIpsumTextLabel.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
    }
    
    @IBAction func joinMSRPressed(_ sender: UIButton) {
        
        getAuthTokenWithWebLogin()
    }
    
    @IBAction func crossButton(_ sender: UIButton) {
        
        
    }
    
    func getAuthTokenWithWebLogin() {
        
        let authURL = URL(string:
                            "http://localhost:3000/default?clientId=TCP&redirectURL=starbuckssso://login-callback")
        let callbackUrlScheme = "starbuckssso"
        
        self.session = ASWebAuthenticationSession.init(url: authURL!, callbackURLScheme: callbackUrlScheme, completionHandler: { (callBack:URL?, error:Error?) in
            
            // handle auth response
            guard error == nil, let successURL = callBack else {
                print(error!)
                return
            }
            
            // print("success")
            
            // print(successURL)
            let queryItems = NSURLComponents(string: (successURL.absoluteString))?.queryItems
            let oauthToken = queryItems?.filter({$0.name == "authCode"}).first
            let codeVerifier = queryItems?.filter({$0.name == "codeVerifier"}).first
            
            
            // Do what you now that you've got the token, or use the callBack URL
            // print(queryItems!)
            // print(oauthToken ?? "No OAuth Token")
            // print(codeVerifier ?? "No code verifier")
            self.getAccessToken(authToken: oauthToken?.value, codeVerif: codeVerifier?.value)
            self.performSegue(withIdentifier: "logIn Segue", sender: self)
        })
        
        self.session?.presentationContextProvider = self
        
        self.session?.start()
    }
    
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        //print(session)
        return  view.window!
    }
    
    
    func getAccessToken(authToken : String? , codeVerif : String?){
        let Url = String(format: "http://localhost:8080/api/v1/access-token/\(String(describing: authToken!))")
        guard let serviceUrl = URL(string: Url) else { return }
        let parameters: [String: Any] = [
            "codeVerifier": "\(String(describing: codeVerif!))"
        ]
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "POST"
        request.setValue("TCP", forHTTPHeaderField: "client_id")
        request.setValue("test", forHTTPHeaderField: "client_secret")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters) else {
            return
        }
        request.httpBody = httpBody
        //request.timeoutInterval = 20
        //print("Hello:", request)
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            
            if let response = response {
                //print(response)
            }
            if let data = data {
                let getData = String(data : data ,encoding: .utf8)
                print(getData!)
                self.parseData(jdata: data)
                
//                self.tokenDelegate?.passData(token: self.btoken)
            }
            
        }.resume()
    }
    
    func parseData(jdata : Data){
        let decoder = JSONDecoder()
        do{
            let decodedData = try decoder.decode(AuthenticationData.self, from: jdata)
            btoken = decodedData.accessToken
        }catch{
            print(error)
           
        }
    }
    
    func loadData(){
        print("load:", btoken )
        tokenDelegate?.passData(token: btoken )
    }
    
}

    
    

