//
//  AuthenticationData.swift
//  Starbucks JMSR
//
//  Created by Shubha Sachan on 09/11/20.
//

import Foundation

struct AuthenticationData : Codable{
    var accessToken : String
}
